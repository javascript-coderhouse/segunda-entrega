# Curso JavaScript Coderhouse

## Clase 13 - Librerías

### Incorporando librerías

---

**Tipo de desafío:** 
Desafío entregable.

**Consigna:** 
Con lo visto en clase, incorpora al menos una librería a tu proyecto, brindándole un uso relevante. Si lo ves necesario, también es válida la opción de investigar por tu cuenta e integrar una librería diferente a las vistas en clase. Cualquiera sea tu elección de librería, justificá tu decisión.

---

**>> Objetivos Generales:**

- Incorporar una librería al proyecto de manera coherente, cuya aplicación se torne significativa para tu proyecto.

- Justificá tu elección según la naturaleza de tu proyecto y la utilidad que la librería seleccionada pueda tener en él. 


**>> Ejemplos:**

- Algunas pistas 😎 Para lograr coherencia entre la librería seleccionada y el proyecto, deberás pensar en su aplicación real. Supongamos:

    - Trabajo con fechas, en este caso lo más útil sería elegir librería Luxon 📆

    - Trabajo con alert, entonces lo más coherente sería tomar Toastify o Sweet Alert 🚨
---

##

